const path = require("path");

exports.createPages = async ({ actions, reporter, graphql }) => {
    const { createPage } = actions;

    const result = await graphql(`
    {
      allMdx {
        edges {
          node {
	    id
            frontmatter {
	      category
              title
              slug
            }
	    internal {
	      contentFilePath
	    }
          }
        }
      }
    }`);
    if (result.errors) {
    	reporter.panicOnBuild(`Error while running GraphQL query.`);
    	return;
    }
    
	const publications = result.data.allMdx.edges.filter(({node: { frontmatter: { category }}}) => category == "publication");
    
    const publicationTemplate = path.resolve(`src/templates/pub.js`);

    publications.forEach(({ node }, index) => {
    	createPage({
    		path: `publications/${node.frontmatter.slug}`,
    		component: `${publicationTemplate}?__contentFilePath=${node.internal.contentFilePath}`,
    		context: {
		    id: node.id,
		},
    	})
    });
}; 
