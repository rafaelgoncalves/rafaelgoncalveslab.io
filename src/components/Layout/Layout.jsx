import React, {useEffect, useState} from "react";
import Helmet from "react-helmet";
import { graphql, useStaticQuery } from "gatsby";
import { Link } from "gatsby";
import "/src/static/styles.scss";
import * as style from "./Layout.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExternalLink } from "@fortawesome/free-solid-svg-icons";
import classnames from "classnames";

const Footer = () => (<footer className={style.footer}><span>{"Rafael Gonçalves | 2024"}</span></footer>);

const Header = () => {
  const menuItems = [
    {
       label: "Sobre",
       link: "/sobre",
       title: "Sobre",
     },
    {
      label: "Maquinações",
      link: "https://maquinacoes.rafaelg.net.br/",
      external: true,
      title:
        "Experimentações teóricas, fichamentos e outros comentários descartáveis",
    },
    // {
    //   label: "Produção",
    //   link: "/producao",
    // },
    // {
    //   label: "Genuary 2022",
    //   link: "https://rafaelgoncalves.gitlab.io/genuary-2022",
    // },
    // {
    //   label: "blog",
    //   link: "/blog",
    // },
  ];

  const [randomX, setRandomX] = useState(undefined);
  const [randomY, setRandomY] = useState(undefined);

  useEffect(() => {
    setRandomX(Math.random());
    setRandomY(Math.random());
  }, []);

  return (
    <header className={style.header} style={{
	    "--randomX": randomX,
	    "--randomY": randomY,
    }}>
      <Link title="Início" to={"/"} ><div className={style.home} /></Link>
      <ul role="menubar">
        {menuItems.map(({ label, link, external, ...rest }, idx) => (
          <li key={`${label}-${idx}`}>
	    {!!external ? (
            <Link to={link} target="_blank" rel="noopener noreferrer" {...rest}>
              {label}
	      <FontAwesomeIcon icon={faExternalLink} size="2xs"/>
            </Link>
	    ) : (
            <Link to={link} {...rest}>
              {label}
            </Link>
	    )}
          </li>
        ))}
      </ul>
    </header>
  );
};

const Layout = React.forwardRef(
  ({ className, header, children, background, supressFooter=false }, ref) => {
    const data = useStaticQuery(graphql`
      query layoutQuery {
        site {
          siteMetadata {
            title
            lang
            author
            description
          }
        }
      }
    `);
    return (
      <>
        <Helmet
          titleTemplate={`%s | ${data.site.siteMetadata.title}`}
          defaultTitle={data.site.siteMetadata.title}
        >
          <html lang={data.site.siteMetadata.lang} />
          <meta charSet="utf-8" />
          <meta name="author" content={data.site.siteMetadata.author} />
          <meta
            name="description"
            content={data.site.siteMetadata.description}
          />
          <meta
            name="icon"
            type="image/x-icon"
            href={data.site.siteMetadata.icon}
          />
        </Helmet>
        <div className={style.bg} id="background" ref={ref}>
          {background}
        </div>
        <div className={style.content}>
          <Header />
          <main className={classnames(className,style.main)}>{children}</main>
		{!supressFooter && <Footer />}
        </div>
      </>
    );
  }
);

export default Layout;
