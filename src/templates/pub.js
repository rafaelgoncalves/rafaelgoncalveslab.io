import React from "react";
import Layout from "../components/Layout";
import { graphql, Link } from "gatsby";
import {GatsbyImage, getImage} from "gatsby-plugin-image";

const styles = {};

const Card = ({children}) => (<div style={{backgroundColor: "black", margin: "2em", borderRadius: "2em", padding: "1em"}}>{children}</div>);

const Publication = ({data, children}) => {
	const { mdx: publication, site } = data;
  	const { siteMetadata } = site;

	console.log(publication);

	const image = getImage(publication.frontmatter.featImage?.childImageSharp?.gatsbyImageData);

	return (<Layout>
		<Card>
			<>
		<h1>{publication.frontmatter.title}</h1>
		{image && (<GatsbyImage image={image} alt="Featured Image"/>)}
		<author>{publication.frontmatter.author}</author>
		{!!publication.frontmatter.link && (<p>Disponível em: <Link to={publication.frontmatter.link} target="_blank" rel="noopener noreferrer">{publication.frontmatter.link}</Link></p>)}
		<p>{publication.frontmatter.abstract}</p>
		{children}
				</>
		</Card>
		</Layout>);
};

export const pageQuery = graphql`
  query PublicationByPath($id: String) {
    mdx(id : { eq: $id }){
      id
      body
      excerpt
      frontmatter {
        date(formatString: "DD/MM/YYYY")
        title
        author
	abstract
	link
	featImage {
          childImageSharp {
	    gatsbyImageData(width: 200)
	  }
	}
      }
    }
    site {
      siteMetadata {
        title
        author
      }
    }
  }
`;


export default Publication;
