module.exports = {
  siteMetadata: {
    siteUrl: "https://rafaelg.net.br",
    title: "rafael gonçalves",
    author: "Rafael Gonçalves",
    lang: "pt-br",
    description: "Agregado da minha presença online",
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
        resolve: "gatsby-plugin-mdx",
	options: {
	   gatsbyRemarkPlugins: [
	       {
	           resolve: "gatsby-remark-images",
		   options: { maxWidth: 1200 },
	       }
	   ]
	}
    },
    "gatsby-plugin-mdx-frontmatter",
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        excerpt_separator: "<!-- more -->",
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              // It's important to specify the maxWidth (in pixels) of
              // the content container as this plugin uses this as the
              // base for generating different widths of each image.
              maxWidth: 1200,
            },
          },
        ],
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "content",
        path: "./content/",
      },
      __key: "content",
    },
  ],
};
